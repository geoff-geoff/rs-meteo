use crate::get_info::{Current,AutreJour};

pub fn print_current(current_condition: Current) {
    println!(r#"actuellement:
condition: {}
il fait: {}°C avec un vent à {}km/h,
avec une temp max de {}°C et min de {}°C
moyenne vent {:.2} Km/h"#, current_condition.condition, current_condition.temp, current_condition.wind, current_condition.tmax, current_condition.tmin, current_condition.moyenne_vent);
}

pub fn print_les_infos(jour: AutreJour) {
    println!();
    println!();

    println!(r#"{} {}   condition: {}
📈 Max: {}°C     📉 Min: {}°C
Moyenne du vent:風  {:.2} Km/h
☔ : {:.2}mm"#, jour.jour, jour.date, jour.condition, jour.tmax, jour.tmin, jour.moyenne_vent, jour.pluie);

}
