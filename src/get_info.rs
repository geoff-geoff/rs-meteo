pub struct Current {
    pub temp: String,
    pub wind: String,
    pub condition: String,
    pub tmax: String,
    pub tmin: String,
    pub moyenne_vent: f64,
}

pub struct AutreJour{
    pub tmax: String,
    pub moyenne_vent: f64,
    pub date: String,
    pub jour: String,
    pub tmin: String,
    pub condition: String,
    pub pluie: f64,
}

pub fn get_current(le_json: &serde_json::Value) -> Current {
     let c = Current{
        temp : le_json["current_condition"]["tmp"].to_string(),
        wind : le_json["current_condition"]["wnd_spd"].to_string(),
        condition: le_json["current_condition"]["condition"].to_string(),
        tmax: le_json["fcst_day_0"]["tmax"].to_string(),
        tmin: le_json["fcst_day_0"]["tmin"].to_string(),
        moyenne_vent: vitesse_vent(&le_json["fcst_day_0"]),
    };
    c
}

pub fn get_jour(le_json: &serde_json::Value) -> AutreJour {
    let c = AutreJour{
         tmax: le_json["tmax"].to_string(),
         moyenne_vent: vitesse_vent(&le_json),
         date: le_json["date"].to_string().replace('"', ""),
         jour: le_json["day_short"].to_string().replace('"', ""),
         tmin: le_json["tmin"].to_string(),
         condition: le_json["condition"].to_string(),
         pluie: cumul_pluie(&le_json),
    };
    c
}


fn vitesse_vent(liste_donnee: &serde_json::Value) -> f64 {
    let mut total: f64 = 0.0; // ne peut etre parser qu'en u64
    let mut iter: f64 = 0.0;

    for elem in 0..24 {
        let g = format!("{}H00", elem);
        let v =  match liste_donnee["hourly_data"][g]["WNDSPD10m"].as_f64() {
            Some(val) => val,
            None => 0.0
            };
        iter += 1.0;
        total += v;
    };

    total/iter
}

fn cumul_pluie(liste_donnee: &serde_json::Value) -> f64 {
    let mut total: f64 = 0.0; // ne peut etre parser qu'en u64

    for elem in 0..24 {
        let g = format!("{}H00", elem);
        let v =  match liste_donnee["hourly_data"][g]["APCPsfc"].as_f64() {
            Some(val) => val,
            None => 0.0
            };
        total += v;
    };
    total
}
