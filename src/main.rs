extern crate reqwest;
extern crate serde_json;
mod get_info;
mod print;

use std::env;
use serde_json::Value;

fn main() {
    //collect arguments  [0] nom programme [1] nom de la ville
    let args: Vec<String> = env::args().collect();

    //on format le lien du JSON
    let ou = format!("https://www.prevision-meteo.ch/services/json/{}", args[1]);

    //on récup le JSON
    let response = reqwest::get(&ou)
        .expect("error url")
        .text()
        .expect("erro text");

    // on parse le JSON en <Value>   array of array
    let jsonifier: Value =   serde_json::from_str(&response).unwrap();


// on parse les données en struct
    let current_condition = get_info::get_current(&jsonifier);

    let demain = get_info::get_jour(&jsonifier["fcst_day_1"]);
    let jour_2 = get_info::get_jour(&jsonifier["fcst_day_2"]);
    let jour_3 = get_info::get_jour(&jsonifier["fcst_day_3"]);
    let jour_4 = get_info::get_jour(&jsonifier["fcst_day_4"]);


// on print les differents jours
    print::print_current(current_condition);

    print::print_les_infos(demain);
    print::print_les_infos(jour_2);
    print::print_les_infos(jour_3);
    print::print_les_infos(jour_4);
}
